package com.example.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.Toast;

public class RestaurarDatos {
    Context context;
    public void main(final Context context) {
        this.context = context;
        MissageDialog missageDialog = new MissageDialog();
        AlertDialog.Builder  alertDialog = missageDialog.createDialog(context, "Restaurar datos", "Estas seguro de que quieres eliminar TODOS los datos?", null);
        alertDialog.setPositiveButton("si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restaurarDatos();
                Toast.makeText(context, "Datos restaurados con exito!", 2000).show();
            }
        });
        alertDialog.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "Datos no restaurados.", 2000).show();
            }
        });

        alertDialog.show();

    }

    // Funcion que restaura los datos
    private void restaurarDatos() {
        ConnectDB connectDB = new ConnectDB(context);
        SQLiteDatabase database = connectDB.getWritableDatabase();
        database.execSQL(ConnectDB.TABLE_DROP);
        database.execSQL(ConnectDB.TABLE_CREATE);
    }
}
