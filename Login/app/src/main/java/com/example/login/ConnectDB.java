package com.example.login;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ConnectDB extends SQLiteOpenHelper {
    
    static String dbName = "database.db";
    static String TABLE_NAME = "user";

    static final String TABLE_CREATE = "CREATE TABLE user(id INTEGER PRIMARY KEY AUTOINCREMENT, user VARCHAR, password VARCHAR)";
    static String TABLE_DROP = "drop table user";


    public ConnectDB(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TABLE_DROP);
    }

}
