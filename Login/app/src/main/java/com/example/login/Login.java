package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    EditText editTextUsuario, editTextPassword;
    ConnectDB connectDB;
    SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsuario = findViewById(R.id.editTextUsuario);
        editTextPassword = findViewById(R.id.editTextPassword);

        // Creo la instancia de la db
        connectDB = new ConnectDB(this);

    }

    // Button login
    public void clickButtonLogin(View view) {
        database = connectDB.getWritableDatabase();
        boolean login = false;
        // Usuario y contraseña que introduce el usuario
        String usuario = editTextUsuario.getText().toString();
        String password = editTextPassword.getText().toString();
        Cursor cursor = database.rawQuery("select * from user", null);
        boolean existe = false;
        if (cursor.moveToFirst()) {
            do {
                existe = true;
                String usuarioDB = cursor.getString(cursor.getColumnIndex("user"));
                String passwordDB = cursor.getString(cursor.getColumnIndex("password"));

                // Login correcto
                if (usuario.equals(usuarioDB) && password.equals(passwordDB)) {
                    login = true;
                }

            } while (cursor.moveToNext());
        }

        database.close();

        // Si no existen datos en la BD
        if (!existe) {
            MissageDialog missageDialog = new MissageDialog();
            AlertDialog.Builder alertDialog = missageDialog.createDialog(this, "ATENCION", "No existe ningun registro en la Base de Datos. Agrega almenos un usuario para poder iniciar sesion.", null);
            alertDialog.setPositiveButton("vale", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    iniciarMenu();
                }
            });
            alertDialog.show();
        }

        // Si el usuario o contraseña son incorrectos
        else if (!login) {
            // Creo una ventane emergente nueva
            MissageDialog missageDialog = new MissageDialog();
            missageDialog.createDialog(this, "Error", "Usuario y/o contraseña incorrecto", "Reintentar").show();
        }

        // Login correcto
        else if (login) {
            // Cierro la ventana actual y abro el menu
            System.out.println("login correcto");
            finish();
            iniciarMenu();
        }

    }

    // Funcion que inicia el menu de la aplicacion
    private void iniciarMenu() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
    }

}