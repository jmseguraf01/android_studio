package com.example.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class ActualizarDatos {
    SQLiteDatabase database;
    ConnectDB connectDB;
    private Context context;
    private int IDUpdate;
    private String usuario, password;
    private ConsultarDatos consultarDatos;

    public void getContext(Context context, ConsultarDatos consultarDatos) {
        this.context = context;
        this.consultarDatos = consultarDatos;
    }

    public void getEditText(int id, String usuario, String password) {
        this.IDUpdate = id;
        this.usuario = usuario;
        this.password = password;
    }

    // Pregunta si desea actualizar los datos
    public void dialogActualizarDatos() {
        MissageDialog missageDialog = new MissageDialog();
        AlertDialog.Builder alertDialog = missageDialog.createDialog(context, "Actualizar datos", "Desea guardar los cambios con los datos actualizados?", null);
        alertDialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                actualizarDatos();
                // Cierro el teclado
                CerrarTeclado.cerrarTeclado(consultarDatos);
                Toast.makeText(context, "Datos actualizados con exito!", 2000).show();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "Datos no actualizados.", 2000).show();
            }
        });
        alertDialog.show();
    }

    // Actualizar datos
    public void actualizarDatos() {
        connectDB = new ConnectDB(context);
        database = connectDB.getWritableDatabase();
        database.execSQL("update "+ConnectDB.TABLE_NAME+" set user = '"+usuario+"', password = '"+password+"' where id = "+IDUpdate+" ");
        database.close();
    }


}
