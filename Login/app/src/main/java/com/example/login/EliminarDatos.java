package com.example.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class EliminarDatos {
    SQLiteDatabase database;
    ConnectDB connectDB;
    private Context context;
    private int IDEliminar;

    public void getContext(Context context) {
        this.context = context;
    }

    public void getID(int id) {
        this.IDEliminar = id;
    }

    // Crea un mensaje de dialogo y pregunta si desea eliminar lso datos
    public void dialogEliminarDatos() {
        MissageDialog missageDialog = new MissageDialog();
        AlertDialog.Builder alertDialog = missageDialog.createDialog(context, "Elimiar datos", "Deseas eliminar los datos seleccionados?", null);
        alertDialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            // ELIMINAR DATOS
            public void onClick(DialogInterface dialog, int which) {
                eliminar();
                Toast.makeText(context, "Datos eliminados con exito!", 2000).show();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            // NO ELIMINAR DATOS
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "Datos no eliminados.", 2000).show();
            }
        });
        alertDialog.show();
    }

    // Funcion que elimina los datos seleccionados en ese momento
    private void eliminar() {
        connectDB = new ConnectDB(context);
        database = connectDB.getWritableDatabase();
        database.execSQL("delete from "+ConnectDB.TABLE_NAME+" where id = '"+IDEliminar+"' ");
        database.close();
    }
}
