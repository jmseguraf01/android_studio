package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText editTextID, editTextUsuario, editTextPassword;
    Button buttonInsert, buttonConsultar;
    ConnectDB connectDB;
    SQLiteDatabase database;
    public static List ID, usuarios, passwords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Conecto EditText
        editTextID = findViewById(R.id.editTextID);
        editTextUsuario = findViewById(R.id.editTextUsuario);
        editTextPassword = findViewById(R.id.editTextPassword);
        // Conecto los botones
        buttonInsert = findViewById(R.id.buttonGuardarDatos);
        buttonConsultar = findViewById(R.id.buttonConsultarDatos);

        // Creo la instancia de la db
        connectDB = new ConnectDB(this);

    }

    // Cargo el menu (los tres botones de opciones)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    // Insertar datos
    public void clickButtonInsert(View view) {
        database = connectDB.getWritableDatabase();
        // Creo la clase
        InsertarDatos insertarDatos = new InsertarDatos();
        // Obtengo los datos que ha introducido el usuario
        String id = editTextID.getText().toString();
        String usuario = editTextUsuario.getText().toString();
        String password = editTextPassword.getText().toString();

        // Siempre que no falte algun dato y no se haya rellenado el ID
        if (!usuario.equals("") && !password.equals("") && id.equals("")) {
            insertarDatos.insert(database, usuario, password);
            // Cierro el teclado
            CerrarTeclado.cerrarTeclado(this);
            // Mensaje de dialogo
            Toast.makeText(this, "Datos guardados con éxito!", 2000).show();
            resetTextEditText();
        }

        // Si se ha rellenado el ID
        else if (!id.equals("")) {
            MissageDialog missageDialog = new MissageDialog();
            missageDialog.createDialog(this, "Error", "El campo ID debe estar vacio.", "Volver").show();
        }

        // Si falta algun dato
        else {
            MissageDialog missageDialog = new MissageDialog();
            AlertDialog.Builder alertDialogBuilder = missageDialog.createDialog(this, "Error", "Los campos usuario y contraseña no pueden estar vacios.", "Volver");
            alertDialogBuilder.show();
        }
        database.close();

    }

    // Funcion que resetea el texto de los editText
    private void resetTextEditText() {
        editTextID.setText("");
        editTextUsuario.setText("");
        editTextPassword.setText("");
    }


    // Consultar datos en la db
    public void clickButtonConsultar(View view) {
        // Creo las listas de Id, usuario y password
        ID = new ArrayList();
        usuarios = new ArrayList();
        passwords = new ArrayList();
        // Conecto con la base de datos
        database = connectDB.getWritableDatabase();

        Cursor cursor = detectEditTextEscrito();
        boolean existe = false;

        // Siempre que hayan datos
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    existe = true;
                    ID.add(cursor.getInt(cursor.getColumnIndex("id")));
                    usuarios.add(cursor.getString(cursor.getColumnIndex("user")));
                    passwords.add(cursor.getString(cursor.getColumnIndex("password")));
                } while (cursor.moveToNext());
                cursor.close();
            }
        }

        database.close();

        // Abro la pantalla de consultas y le paso los datos que he sacado de la BBDD
        if (existe) {
            Intent intent = new Intent(this, ConsultarDatos.class);
            this.startActivity(intent);
        }
        // Si no hay datos con esas caracteristicas
        else {
            MissageDialog missageDialog = new MissageDialog();
            missageDialog.createDialog(this, "Error!", "No existen datos con estas caracteristicas.", "Volver").show();
        }
    }

    // Funcion que comprueba si esta escrito el ID, usuario o password y hace la consulta
    private Cursor detectEditTextEscrito() {
        Cursor cursor = null;
        String id = editTextID.getText().toString();
        String usuario = editTextUsuario.getText().toString();
        String passwd = editTextPassword.getText().toString();
        // Solo ID
        if (!id.equals("") && usuario.equals("") && passwd.equals("")) {
            System.out.println("solo id");
            cursor = database.rawQuery("select * from "+ConnectDB.TABLE_NAME+" where id LIKE '"+id+"'", null);
        }
        // ID + Usuario
        else if (!id.equals("") && !usuario.equals("") && passwd.equals("")) {
            System.out.println("id y usuario");
            cursor = database.rawQuery("select * from "+ConnectDB.TABLE_NAME+" where id LIKE '"+id+"' and user LIKE '%"+usuario+"%' ", null);
        }
        // ID + usuario + password
        else if (!id.equals("") && !usuario.equals("") && !passwd.equals("")) {
            System.out.println("id y usuario y passwd");
            cursor = database.rawQuery("select * from "+ConnectDB.TABLE_NAME+" where id LIKE '"+id+"' and user LIKE '%"+usuario+"%' and password LIKE '%"+passwd+"%'", null);
        }
        // Solo usuario
        else if (id.equals("") && !usuario.equals("") && passwd.equals("")) {
            System.out.println("solo usuario");
            cursor = database.rawQuery("select * from "+ConnectDB.TABLE_NAME+" where user LIKE '%"+usuario+"%'", null);
        }
        // Usuario + password
        else if (id.equals("") && !usuario.equals("") && !passwd.equals("")) {
            System.out.println("usuario y password");
            cursor = database.rawQuery("select * from "+ConnectDB.TABLE_NAME+" where user LIKE '%"+usuario+"%' and password LIKE '%"+passwd+"%'", null);
        }
        // Solo password
        else if (id.equals("") && usuario.equals("") && !passwd.equals("")) {
            System.out.println("solo password");
            cursor = database.rawQuery("select * from "+ConnectDB.TABLE_NAME+" where password LIKE '%"+passwd+"%' ", null);
        }

        return cursor;
    }

    // Cuando se da click en cualquier item del MENU
    public void clickItemMenu(MenuItem item) {
        // Si es el item de restaurar Datos
        if (item.getItemId() == R.id.itemRestaurarDatos) {
            RestaurarDatos restaurarDatos = new RestaurarDatos();
            restaurarDatos.main(this);
        }
    }

}