package com.example.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class MissageDialog {

    // Creo el mensaje de dialogo
    AlertDialog.Builder createDialog(Context context, String dialogTitle, String dialogMessage, String dialogButtonText) {
        final AlertDialog.Builder missageDialog = new AlertDialog.Builder(context);
        missageDialog.setTitle(dialogTitle);
        missageDialog.setMessage(dialogMessage);
        missageDialog.setPositiveButton(dialogButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return missageDialog;
    }

}
