package com.example.login;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class CerrarTeclado {
    // Funcion que cierra el teclado
    public static void cerrarTeclado(Activity mainActivity) {
        View view = mainActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) mainActivity.getSystemService(mainActivity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
