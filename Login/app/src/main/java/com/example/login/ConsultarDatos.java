package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ConsultarDatos extends AppCompatActivity {

    private int posicion = 0;

    EditText editTextID, editTextUsuario, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_consultar_datos);
        // Enlazo
        editTextID = findViewById(R.id.editTextID);
        editTextUsuario = findViewById(R.id.editTextUsuario);
        editTextPassword = findViewById(R.id.editTextPassword);
        mostarDatos();
    }


    // Funcion que muestra los datos
    public void mostarDatos() {
        editTextID.setText(MainActivity.ID.get(posicion).toString());
        editTextUsuario.setText(MainActivity.usuarios.get(posicion).toString());
        editTextPassword.setText(MainActivity.passwords.get(posicion).toString());
    }

    // Click en el boton anterior
    public void clickButtonAnterior(View view) {
        // Compruebo si es igual o mayor que la posicion del primer dato
        if (posicion - 1 >= 0) {
            posicion = posicion - 1;
            mostarDatos();
        } else {
            Toast.makeText(this, "No hay mas datos anteriores.", 2000).show();
        }
    }

    // Click en el boton siguiente
    public void clickButtonSiguiente(View view) {
        // Compruebo que sea menor o igual que la posicion del ultimo dato
        if (posicion + 1 < MainActivity.usuarios.size()) {
            posicion = posicion + 1;
            mostarDatos();
        } else {
            Toast.makeText(this, "No hay mas datos posteriores.", 2000).show();
        }
    }

    // Click boton eliminar datos
    public void clickButtonEliminar(View view) {
        EliminarDatos eliminarDatos = new EliminarDatos();
        eliminarDatos.getContext(this);
        eliminarDatos.getID(Integer.parseInt(editTextID.getText().toString()));
        eliminarDatos.dialogEliminarDatos();
    }

    // Click boton actualizar datos
    public void clickButtonActualizar(View view) {
        ActualizarDatos actualizarDatos = new ActualizarDatos();
        // Cojo los datos actualizados y los paso
        int id = Integer.parseInt(editTextID.getText().toString());
        String usuario = editTextUsuario.getText().toString();
        String password = editTextPassword.getText().toString();
        // Paso los datos
        actualizarDatos.getContext(this, this);
        actualizarDatos.getEditText(id, usuario, password);
        actualizarDatos.dialogActualizarDatos();
    }

}