package com.example.jusef.tresenraya;

import java.util.Arrays;

public class Game {
    // Creo un array en el que guardo el tablero
    String[] tablero = new String[9];

    String turno = "";


    // Funcion para volver el a poner el tablero a inicio
    void restaurarTablero() {
        for (int i = 0; i < 9; i++) {
            tablero[i] = "";
        }
    }

    // Relleno el tablero
    void rellenarTablero(int x, String turno) {
        // Actualizo turno
        this.turno = turno;
        for (int i = 0; i < 9; i++) {
            // Cuando la posicion es la misma que x, lo relleno
            if (i == x) {
                tablero[x] = turno;
            }
        }
    }

    // Funcion donde compruebo si alguien ha ganado
    String comprobarGanador() {
        // String que retorno
        String textoResultado  = "";

        // Primera linea horizontal
        // Siempre que sean iguales pero no esten vacios
        if (!tablero[0].equals("") && !tablero[1].equals("") && !tablero[2].equals("")) {
            if (tablero[0].equals(tablero[1]) && tablero[1].equals(tablero[2])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Segunda linea horizontal
        if (!tablero[3].equals("") && !tablero[4].equals("") && !tablero[5].equals("")) {
            if (tablero[3].equals(tablero[4]) && tablero[4].equals(tablero[5])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Tercera linea horizontal
        if (!tablero[6].equals("") && !tablero[7].equals("") && !tablero[8].equals("")) {
            if (tablero[6].equals(tablero[7]) && tablero[7].equals(tablero[8])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Primera linea vertical
        if (!tablero[0].equals("") && !tablero[3].equals("") && !tablero[6].equals("")) {
            if (tablero[0].equals(tablero[3]) && tablero[3].equals(tablero[6])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Segunda linea vertical
        if (!tablero[1].equals("") && !tablero[4].equals("") && !tablero[7].equals("")) {
            if (tablero[1].equals(tablero[4]) && tablero[4].equals(tablero[7])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Tercera linea vertical
        if (!tablero[2].equals("") && !tablero[5].equals("") && !tablero[8].equals("")) {
            if (tablero[2].equals(tablero[5]) && tablero[5].equals(tablero[8])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Diagonal izquierda a derecha
        if (!tablero[0].equals("") && !tablero[4].equals("") && !tablero[8].equals("")) {
            if (tablero[0].equals(tablero[4]) && tablero[4].equals(tablero[8])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Diagonal derecha a izquierda
        if (!tablero[2].equals("") && !tablero[4].equals("") && !tablero[6].equals("")) {
            if (tablero[2].equals(tablero[4]) && tablero[4].equals(tablero[6])) {
                textoResultado = "Ganador: " + turno;
            }
        }

        // Si estan todos llenos, hay un empate
        boolean todosLlenos = true;
        for (int i = 0; i < 9; i++) {
            // Si hay algun tablero vacio
            if (tablero[i].equals("")) {
                todosLlenos = false;
            }
        }

        if (todosLlenos) {
            textoResultado = "Empate!!";
        }

        return textoResultado;

    }
}
