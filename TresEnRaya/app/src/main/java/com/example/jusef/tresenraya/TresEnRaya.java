package com.example.jusef.tresenraya;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class TresEnRaya extends AppCompatActivity {

    // Creo los botones de java
    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, bReset;
    String turno = "X";
    TextView labelGanador;
    ImageView imagenTrofeo;

    // Convoco la clase game
    Game game = new Game();

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tres_en_raya);

        // Enlazo los botones de java con los del android
        b1 = findViewById(R.id.button1);
        b2 = findViewById(R.id.button2);
        b3 = findViewById(R.id.button3);
        b4 = findViewById(R.id.button4);
        b5 = findViewById(R.id.button5);
        b6 = findViewById(R.id.button6);
        b7 = findViewById(R.id.button7);
        b8 = findViewById(R.id.button8);
        b9 = findViewById(R.id.button9);
        bReset = findViewById(R.id.buttonReset);
        labelGanador = findViewById(R.id.textViewGanador);
        imagenTrofeo = findViewById(R.id.imageTrofeo);

        // Reseteo los botones
        resetGame();
        // Reseteo el tablero del juego
        game.restaurarTablero();

    }


    // Cuando se le da click a cualquier boton
    public void clickButons(View view) {
        boolean casillaVacia = false;
        // Buton 1
        if (view.getId() == R.id.button1) {
            // Siempre que este vacia la casilla
            if (b1.getText().equals("")) {
                b1.setText(turno);
                // Le paso la posicion del tablero y el turno que le toca
                game.rellenarTablero(0, turno);
                casillaVacia = true;
            }
        }

        // Button 2
        else if (view.getId() == R.id.button2) {
            if (b2.getText().equals("")) {
                b2.setText(turno);
                game.rellenarTablero(1, turno);
                casillaVacia = true;
            }
        }

        // Buton 3
        else if (view.getId() == R.id.button3) {
            if (b3.getText().equals("")) {
                b3.setText(turno);
                game.rellenarTablero(2, turno);
                casillaVacia = true;
            }
        }

        // Buton 4
        else if (view.getId() == R.id.button4) {
            if (b4.getText().equals("")) {
                b4.setText(turno);
                game.rellenarTablero(3, turno);
                casillaVacia = true;
            }
        }

        // Button 5
        else if (view.getId() == R.id.button5) {
            if (b5.getText().equals("")) {
                b5.setText(turno);
                game.rellenarTablero(4, turno);
                casillaVacia = true;
            }
        }

        // Button 6
        else if (view.getId() == R.id.button6) {
            if (b6.getText().equals("")) {
                b6.setText(turno);
                game.rellenarTablero(5, turno);
                casillaVacia = true;
            }
        }

        // Button 7
        else if (view.getId() == R.id.button7) {
            if (b7.getText().equals("")) {
                b7.setText(turno);
                game.rellenarTablero(6, turno);
                casillaVacia = true;
            }
        }

        // Button 8
        else if (view.getId() == R.id.button8) {
            if (b8.getText().equals("")) {
                b8.setText(turno);
                game.rellenarTablero(7, turno);
                casillaVacia = true;
            }
        }

        // Button 9
        else if (view.getId() == R.id.button9) {
            if (b9.getText().equals("")) {
                b9.setText(turno);
                game.rellenarTablero(8, turno);
                casillaVacia = true;
            }
        }

        // Cuando la casilla esta vacia, cambio el turno
        if (casillaVacia) {
            turno = turno.equals("X") ? "O" : "X";
        }

        // Compruebo si hay ganador
        String textoResultado = game.comprobarGanador();
        // Si la funcion me ha devuelto algun ganador
        if (!textoResultado.equals("")) {
            hayGanador(textoResultado);
        }
    }

    // Funcion de cuando hay un ganador
    void hayGanador(String textoResultado) {
        labelGanador.setVisibility(labelGanador.VISIBLE);
        labelGanador.setText(textoResultado);
        // Marco toodos los botones como que no se pueden clickar
        b1.setClickable(false);
        b2.setClickable(false);
        b3.setClickable(false);
        b4.setClickable(false);
        b5.setClickable(false);
        b6.setClickable(false);
        b7.setClickable(false);
        b8.setClickable(false);
        b9.setClickable(false);
        // Imagen del trofeo
        imagenTrofeo.setVisibility(imagenTrofeo.VISIBLE);
        bReset.setText("Volver a jugar");
    }

    // Cuando se le da click al boton Reset
    public void clickReset(View view) {
        resetGame();
    }

    // Funcion para resetear el texto de los botones
    void resetGame() {
        // Click on
        b1.setClickable(true);
        b2.setClickable(true);
        b3.setClickable(true);
        b4.setClickable(true);
        b5.setClickable(true);
        b6.setClickable(true);
        b7.setClickable(true);
        b8.setClickable(true);
        b9.setClickable(true);
        // Texto vacio
        b1.setText("");
        b2.setText("");
        b3.setText("");
        b4.setText("");
        b5.setText("");
        b6.setText("");
        b7.setText("");
        b8.setText("");
        b9.setText("");
        bReset.setText("Reset");
        // Reseteo el turno
        turno = "X";
        // Reseteo el array
        game.restaurarTablero();
        labelGanador.setVisibility(labelGanador.INVISIBLE);
        imagenTrofeo.setVisibility(imagenTrofeo.INVISIBLE);
    }

}
