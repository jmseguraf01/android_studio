package com.aplication.ahorcadov2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class PantallaInicio extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_inicio);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent pantallaJuego = new Intent(PantallaInicio.this, Main.class);
                startActivity(pantallaJuego);
            }
        }, 3000);
    }
}
