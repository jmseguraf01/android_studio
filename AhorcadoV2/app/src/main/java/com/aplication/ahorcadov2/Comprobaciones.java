package com.aplication.ahorcadov2;

import android.content.res.ColorStateList;
import android.graphics.Color;

public class Comprobaciones {

    // Funcion que comprueba si la letra existe en la palabra
    static boolean comprobarLetra(String letra) {
        boolean existe = false;
        // Si la palabra contiene la letra
        if (Palabras.palabraEscogida.contains(letra)) {
            // Busco en que posicion esta la letra
            for (int i = 0; i < Palabras.palabraEscogida.length(); i++) {
                // Si la letra coincide con esta posicion de la palabra
                if (Palabras.palabraEscogida.charAt(i) == letra.charAt(0)) {
                    // Actualizo la posicion del array
                    Palabras.palabraArray[i] = String.valueOf(letra.toString().charAt(0));
                    existe = true;
                }
            }
        }
        int ultimaPosicion = Letras.botonesClickados.size() - 1;
        // Cambio el color del fondo del boton
        if (existe) {
            Letras.botonesClickados.get(ultimaPosicion).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF7DCF1D")));

        } else {
            Letras.botonesClickados.get(ultimaPosicion).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FFAC0303")));
        }
        return existe;
    }
    
    
    // Funcion que comprueba si la palabra esta completa o no
    static boolean comprobarPalabra() {
        boolean palabraCompleta = true;
        for (int i = 0; i < Palabras.palabraEscogida.length(); i++) {
            if (Palabras.palabraEscogida.charAt(i) != Palabras.palabraArray[i].charAt(0)) {
                palabraCompleta = false;
            }
        }
        return palabraCompleta;
    }
}
