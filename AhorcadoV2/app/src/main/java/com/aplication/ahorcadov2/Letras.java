package com.aplication.ahorcadov2;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class Letras {
    // Lista para guardar los botones clickados
    static List<Button> botonesClickados = new ArrayList<Button> ();

    // Funcion que restaura los botones a modo de inicio
    static void restaurarBotones() {
        for (int i = 0; i < botonesClickados.size(); i++) {
            // Le pongo el color de fondo
            botonesClickados.get(i).setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#d6d7d7")));
        }
        // Borro la lista
        botonesClickados.clear();
    }

    // Funcion que comprueba si el boton ya ha sido clickado anteriormente
    static boolean comprobarClick(Button botonClickado) {
        boolean haSidoClickado = false;
        for (int i = 0; i < botonesClickados.size(); i++) {
            if (botonesClickados.get(i) == botonClickado) {
                haSidoClickado = true;
            }
        }

        return haSidoClickado;
    }

}

