package com.aplication.ahorcadov2;

import android.graphics.Color;

import java.util.Arrays;

public class Juego {
    static int intentos = 0;
    // Variable que cambia cuando se finaliza el juego, al perder el usuario
    static boolean finJuego = false;


    // Funcion que escoje una palabra aleatoria
    static void escojerPalabra() {
        System.out.println(Palabras.palabrasYaEscogidas);
        int numeroRandom = (int) (Math.random()* Palabras.palabras.length);
        // Compruebo que la palabra no haya sido ya escogida antes
        if (Palabras.palabrasYaEscogidas.size() > 0 && (Palabras.palabras.length > Palabras.palabrasYaEscogidas.size()) ) {
            boolean existePalabra = true;
            while (existePalabra) {
                for (int i = 0; i < Palabras.palabrasYaEscogidas.size(); i++) {
                    // Si la palabra ya ha sido utilizada, la cambio por otra y compruebo la nueva
                    if (Palabras.palabrasYaEscogidas.get(i).equals(Palabras.palabras[numeroRandom])) {
                        existePalabra = true;
                        numeroRandom = (int) (Math.random()* Palabras.palabras.length);
                        break;
                    } else {
                        existePalabra = false;
                    }
                }
            }
        }
        Palabras.palabraEscogida = Palabras.palabras[numeroRandom];
        // Añado la palabra a la lista de palabras escogidas
        Palabras.palabrasYaEscogidas.add(Palabras.palabras[numeroRandom]);
        // Actualizo la longitud del array
        Palabras.palabraArray = new String[Palabras.palabraEscogida.length()];
    }


    // Funcion que pinta la palabra en el formato de ahorcado
    static void pintarPalabra() {
        Main.textoPalabra.setText("");
        // Por cada palabra pongo un "_" y lo guardo en palabramostrar
        for (int i = 0; i < Palabras.palabraEscogida.length(); i++) {
            Palabras.palabraArray[i] = "__";
        }
        Main.textoPalabra.setText(Arrays.toString(Palabras.palabraArray).toString()
                .replace("[", "").replace("]", ""));
    }


    // Funcion que se llama cuando falla el jugador en la letra y retorna si es el fin del juego
    static void fallo() {
        Juego.intentos++;
        /* Actualizo la imagen
         -------------------- */
        // Si tiene un punto, lo pongo visible
        if (Juego.intentos == 1) {
            Main.imagenAhorcado.setVisibility(Main.imagenAhorcado.VISIBLE);
        }
        else if (Juego.intentos == 2) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado2);
        }
        else if (Juego.intentos == 3) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado3);
        }
        else if (Juego.intentos == 4) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado4);
        }
        else if (Juego.intentos == 5) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado5);
        }
        else if (Juego.intentos == 6) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado6);
        }
        else if (Juego.intentos == 7) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado7);
        }
        else if (Juego.intentos == 8) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado8);
        }
        // Ultimo fallo, falla la palabra completa
        else if (Juego.intentos == 9) {
            Main.imagenAhorcado.setImageResource(R.drawable.ahorcado);
            // Añado un error a la etiqueta de errores
            int numeroAnterior = Integer.parseInt(Main.textoNumeroFallos.getText().toString());
            Main.textoNumeroFallos.setText(String.valueOf(numeroAnterior + 1));
            // Muestro el boton de volver a jugar
            Main.botonVolverAjugar.setVisibility(Main.botonVolverAjugar.VISIBLE);
            finJuego = true;
            // Llamo a la funcion para que reproduzca el sonido de perdedor
            Sonidos.reproducirSonido(false, false, true);
            Main.textoPalabra.setText(Palabras.palabraEscogida);
            Main.textoPalabra.setTextColor(Color.parseColor("#FFAC0303"));
        }

    }



}
