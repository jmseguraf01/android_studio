package com.aplication.ahorcadov2;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Arrays;

public class Main extends AppCompatActivity {
    static TextView textoPalabra, textoNumeroAciertos, textoNumeroFallos;
    static ImageView imagenAhorcado;
    static Button botonVolverAjugar, botonDesactivarSonido;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Para ocultar la barra de arriba
        getSupportActionBar().hide();
        // Uno todos los componentes al layout de android
        textoPalabra = findViewById(R.id.textPalabra);
        textoNumeroAciertos = findViewById(R.id.textViewNumeroAciertos);
        textoNumeroFallos = findViewById(R.id.textViewNumeroFallos);
        imagenAhorcado = findViewById(R.id.imageAhorcado);
        botonVolverAjugar = findViewById(R.id.buttonVolverAJugar);
        botonDesactivarSonido = findViewById(R.id.buttonDesactivarSonido);
        Sonidos.agregarSonidos(this);
        iniciarJuego();
    }

    // Funcion que inicia y resetea el juego
    void iniciarJuego() {
        // Pongo la imagen de 0 y invisible
        imagenAhorcado.setVisibility(imagenAhorcado.INVISIBLE);
        imagenAhorcado.setImageResource(R.drawable.ahorcado1);
        // Oculto el boton de volver a jugar
        botonVolverAjugar.setVisibility(botonVolverAjugar.INVISIBLE);
        Juego.escojerPalabra();
        Juego.pintarPalabra();
    }


    // Funcion al clickar en una letra
    public void clickLetra(View view) {
        // Compruebo si el boton ya ha sido clickado
        boolean haSidoClickado = Letras.comprobarClick((Button)view);
        // Siempre quie no se haya finalizado el juego ni haya sido clickado
        if (!Juego.finJuego && !haSidoClickado) {
            // Obtengo la leta del texto del boton y se la paso a la funcion
            String letra = ((Button) view).getText().toString().toLowerCase();
            // Añado a la lista de botones clickados
            Letras.botonesClickados.add((Button) view);

            boolean existeLetra = Comprobaciones.comprobarLetra(letra);
            boolean palabraCompleta = Comprobaciones.comprobarPalabra();
            // Llamo a la funcion que reproduce los sonidos
            Sonidos.reproducirSonido(existeLetra, palabraCompleta, false);

            // Si la letra existe y no esta completa la palabra, actualizo el texto
            if (existeLetra && !palabraCompleta) {
                textoPalabra.setText(Arrays.toString(Palabras.palabraArray).toString()
                        .replace("[", "").replace("]", ""));
            }
            // Si la palabra esta completa ha ganado !!!
            else if (palabraCompleta) {
                textoPalabra.setText(Palabras.palabraEscogida);
                textoPalabra.setTextColor(Color.parseColor( "#006600"));
                botonVolverAjugar.setVisibility(botonVolverAjugar.VISIBLE);
                // Le añado un acierto a la label de aciertos
                int numeroAnterior = Integer.parseInt(textoNumeroAciertos.getText().toString());
                textoNumeroAciertos.setText(String.valueOf(numeroAnterior + 1));
            }
            // Si no existe la letra
            else {
                Juego.fallo();
            }
        }
    }

    // Click en desactivar sonido
    public void clickSonido(View view) {
        // Si el sonido esta activado lo desactivo
        if (Sonidos.sonido) {
            botonDesactivarSonido.setBackgroundResource(R.drawable.sonido_off);
            Sonidos.sonido = false;
        } else {
            botonDesactivarSonido.setBackgroundResource(R.drawable.sonido_on);
            Sonidos.sonido = true;
        }
    }

    // Click en boton volver a jugar
    public void clickJugar(View view) {
        // Reseteo las variables
        Juego.intentos = 0;
        Juego.finJuego = false;
        textoPalabra.setTextColor(Color.BLACK);
        // Restauro los botones clickados a enabled
        Letras.restaurarBotones();
        // Llamo a la funcion que inicia el juego
        iniciarJuego();
    }
}
