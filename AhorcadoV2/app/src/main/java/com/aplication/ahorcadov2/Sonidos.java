package com.aplication.ahorcadov2;

import android.media.MediaPlayer;

public class Sonidos {
    // Creo los sonidos
    static MediaPlayer sonidoCorrecto, sonidoIncorrecto, sonidoGanador, sonidoPerdido;
    static boolean sonido = true;

    // Funcion que agrega los sonidos, unicamente al iniciar el juego
    static void agregarSonidos(Main main) {
        // Pongo los sonidos
        Sonidos.sonidoCorrecto = MediaPlayer.create(main, R.raw.correcto);
        Sonidos.sonidoIncorrecto = MediaPlayer.create(main, R.raw.incorrecto);
        Sonidos.sonidoGanador = MediaPlayer.create(main, R.raw.ganador);
        Sonidos.sonidoPerdido = MediaPlayer.create(main, R.raw.perdido);
    }

    // Funcion que comprueba y elige que sonido poner
    static void reproducirSonido(boolean existeLetra, boolean palabraCompleta, boolean hasPerdido) {
        // Siempre que el sonido este activado
        if (sonido) {
            if (existeLetra && !palabraCompleta) {
                sonidoCorrecto.start();
            } else if (palabraCompleta) {
                sonidoGanador.start();
            } else if (hasPerdido) {
                sonidoPerdido.start();
            } else {
                sonidoIncorrecto.start();
            }
        }

    }
}
