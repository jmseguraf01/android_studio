package com.example.ahorcado;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    // Defino los botones y texto
    static Button comprobarLetra, comprobarPalabra;
    static TextView textoPalabra, textoTitulo;
    static EditText inputPalabra;
    static ImageView imagenAhorcado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Enlazo los botones y los textos
        comprobarLetra = findViewById(R.id.buttonComprobarLetra);
        comprobarPalabra = findViewById(R.id.buttonComprobarPalabra);
        textoPalabra = findViewById(R.id.textPalabra);
        textoTitulo = findViewById(R.id.textoTitulo);
        inputPalabra = findViewById(R.id.editText);
        imagenAhorcado = findViewById(R.id.imageAhorcado);
        printPalabra();
    }

    // PRIMERA FUNCION, pinta la palabra que se escoje
    void printPalabra() {
        inputPalabra.setText("");
        // Llamo a la funcion para que escoja una palabra
        Game.escojerPalabras();
        textoPalabra.setText("");
        // Por cada palabra pongo un "_" y lo guardo en palabramostrar
        for (int i = 0; i < Palabras.palabraEscogida.length(); i++) {
            Palabras.palabraArray[i] = "__";
        }
        textoPalabra.setText(Arrays.toString(Palabras.palabraArray).toString()
                .replace("[", "").replace("]", ""));
        System.out.println(Palabras.palabraEscogida);
    }

    // Funcion que comprueba la letra
    public void comprobarLetra(View view) {
        // Le paso a la funcion comprobarLetra de palabra la letra introducida en "inputpalabra"
        boolean existe = Game.comprobarLetra(inputPalabra.getText().toString().toLowerCase());
        // Si existe la letra en la palabra
        if (existe) {
            textoPalabra.setText(Arrays.toString(Palabras.palabraArray).toString()
                    .replace("[", "").replace("]", ""));
        }

        // Ha fallado
        else {
            Game.actulizarImagen();
        }

        inputPalabra.setText("");

    }

    // Comprobar la palabra
    public void comprobarPalabra(View view) {
        boolean palabraAcertada = Game.comprobarPalabra(inputPalabra.getText());
        System.out.println(palabraAcertada);
        // Palabra acertada
        if (palabraAcertada) {
            textoPalabra.setText(inputPalabra.getText());
            Game.finJuego("ganado");
        }

        // Ha fallado
        else {
            Game.actulizarImagen();
        }
    }

    // Funcion para volver a jugar
    public void gameAgain(View view) {
        // Actualizo la imagen
        imagenAhorcado.setImageResource(R.drawable.ahorcado0_2);
        comprobarPalabra.setClickable(true);
        comprobarLetra.setClickable(true);
        inputPalabra.setClickable(true);
        // Reseteo los colores del titulo
        textoTitulo.setTextColor(Color.parseColor("#8B000000"));
        // Intentos reset
        Palabras.intentos = 0;
        MainActivity.textoTitulo.setText("La palabra es la siguiente:");
        printPalabra();
    }

}
