package com.example.ahorcado;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.Editable;
import android.widget.EditText;

public class Game {

    // Funcion que escoje una palabra aleatoria
    static void escojerPalabras() {
        int numeroRandom = (int) (Math.random()* Palabras.palabras.length);
        // Guardo la funcion en esa variable
        Palabras.palabraEscogida = Palabras.palabras[numeroRandom];
        // Palabras.palabraEscogida = "hola";
        // Actualizo la longitud del array
        Palabras.palabraArray = new String[Palabras.palabraEscogida.length()];
    }

    // Funcion que comprueba si existe la letra en la palabra
    static boolean comprobarLetra(String letra) {
        boolean existe = false;
        // Siempre que no este vacio el texto
        if (!letra.toString().isEmpty()) {
            // Busco en que posicion esta la letra
            for (int i = 0; i < Palabras.palabraEscogida.length(); i++) {
                // Si la letra coincide con esta posicion de la palabra
                if (Palabras.palabraEscogida.charAt(i) == letra.charAt(0)) {
                    // Actualizo la posicion del array
                    Palabras.palabraArray[i] = String.valueOf(letra.toString().charAt(0));
                    existe = true;
                }
            }
        }
        // Return
        if (existe) return true;
        else return false;
    }


    // Funcion que comprueba si la palabra es la misma
    static boolean comprobarPalabra(Editable palabra) {
        boolean palabraAcertada = false;
        if (palabra.toString().toLowerCase().equals(Palabras.palabraEscogida)) {
            for (int i = 0; i < Palabras.palabraEscogida.length(); i++) {
               Palabras.palabraArray[i] = String.valueOf(Palabras.palabraEscogida.charAt(i));
               palabraAcertada = true;
            }
        }
        if (palabraAcertada) return true;
        else return false;
    }

    // Funcion que se le llama cuando ha perdido y se actualiza la imagen
    static void actulizarImagen() {
        Palabras.intentos++;
        if (Palabras.intentos == 1) {
            MainActivity.imagenAhorcado.setImageResource(R.drawable.ahorcado1);
        }
        else if (Palabras.intentos == 2) {
            MainActivity.imagenAhorcado.setImageResource(R.drawable.ahorcado2);
        }
        else if (Palabras.intentos == 3) {
            MainActivity.imagenAhorcado.setImageResource(R.drawable.ahorcado3);
        }
        else if (Palabras.intentos == 4) {
            MainActivity.imagenAhorcado.setImageResource(R.drawable.ahorcado4);
        }
        else if (Palabras.intentos == 5) {
            MainActivity.imagenAhorcado.setImageResource(R.drawable.ahorcado5);
        }
        else if (Palabras.intentos == 6) {
            MainActivity.imagenAhorcado.setImageResource(R.drawable.ahorcado6);
            finJuego("perdido");
        }

    }

    // Funcion de cuando has perdido
    static void finJuego(String resultado) {
        Palabras.intentos = 0;
        MainActivity.comprobarLetra.setClickable(false);
        MainActivity.comprobarPalabra.setClickable(false);
        MainActivity.inputPalabra.setClickable(false);
        // Si ha ganado
        if (resultado.equals("ganado")) {
            MainActivity.imagenAhorcado.setImageResource(R.drawable.winner);
            MainActivity.textoTitulo.setText("Has acertado!");
            MainActivity.textoTitulo.setTextColor(Color.parseColor("#208000"));
        }
        // Si has perdido
        else {
            MainActivity.textoTitulo.setText("Has perdido!");
            MainActivity.textoTitulo.setTextColor(Color.parseColor("#990000"));
            MainActivity.textoPalabra.setText(Palabras.palabraEscogida);
        }
    }

}
