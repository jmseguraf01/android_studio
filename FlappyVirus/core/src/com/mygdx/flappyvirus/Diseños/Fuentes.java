package com.mygdx.game.Diseños;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Fuentes {
    public BitmapFont fuente = new BitmapFont(Gdx.files.internal("press_start_2p.fnt"), Gdx.files.internal("press_start_2p.png"), false);

    // REFACTOR para que cada vez que se especifique una fuente, obligatoriamente se pide el tamaño de la fuente
    public Fuentes(float scaleX, float scaleY) {
        fuente.getData().setScale(scaleX, scaleY);
    }
}