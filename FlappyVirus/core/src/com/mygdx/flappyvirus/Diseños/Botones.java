package com.mygdx.game.Diseños;

import com.mygdx.game.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class Botones {
    public static int numeroBotones = 3;

    public TextButton crearBotones(String textButton, int widthButton, int heightButton, int x, int y, float scaleButton, Stage stage, Skin skin) {
        TextButton button = new TextButton(textButton , skin);
        button.setPosition(x,y);
        button.setWidth(widthButton);
        button.setHeight(heightButton);
        // Pongo el texto mas grande
        button.getLabel().setFontScale(scaleButton);
        stage.addActor(button);
        return button;
    }

    // Muestro el stage del boton que me pase
    public static void mostrarStage(int stage) {
        Gdx.input.setInputProcessor(MyGdxGame.stages[stage]);
        MyGdxGame.stages[stage].draw();
    }

}
