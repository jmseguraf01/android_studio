package com.mygdx.game.Objetos;

import com.mygdx.game.*;

import com.badlogic.gdx.Gdx;

public class Nubes {
    public static int numeroNubes = 3;
    public static int nubesWidth = 200;
    public static int nubesHeight = 150;
    public static int nubesY = MyGdxGame.pantallaHeight - nubesHeight;
    public static int[] nubesX = new int[numeroNubes];
    public static int espacioEntreNubes = 500;

    // Cada vez se ma moviendo la nube
    public static void mover() {
        for (int i = 0; i < Nubes.numeroNubes; i++) {
            Nubes.nubesX[i] -= 4;
        }
    }

    // Compruebo si la nube ha salido de la pantalla
    public static void comprobarSalePantalla() {
        for (int i = 0; i < Nubes.numeroNubes; i++) {
            if (Nubes.nubesX[i] < -Nubes.nubesWidth) {
                Nubes.nubesX[i] = MyGdxGame.pantallaWidth;
            }
        }
    }

}
