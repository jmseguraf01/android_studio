package com.mygdx.game.Objetos;

public class Cesped {
    public static int[] cespedX = {0,1000};

    public static void moverComprobar() {
        // Muevo el cesped y compruebo si sale de la pantalla
        for (int i = 0; i < cespedX.length; i++) {
            cespedX[i] -= 5;
            // Si llega a la posicion 0, detecto que cesped es y al otro lo pongo a 1000
            if (cespedX[i] == 0) {
                if (i == 0) {
                    cespedX[i + 1] = 1000;
                } else {
                    cespedX[i - 1] = 1000;
                }
            }
        }
    }

}
