package com.mygdx.game.Objetos;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.*;

public class Tubos {
    // Variable de los tubos
    public static int numeroTubos = 3;
    public static float[] tuboX = new float[numeroTubos];
    public static float[] tuboY = new float[numeroTubos];
    public static int espacioEntreTubos = 360;
    public static int separacionHeightTubos = 290;
    public static int widthTubos = 80;
    public static int heightTubos = MyGdxGame.pantallaHeight - separacionHeightTubos;
    // Creo un array porque cada tubo sobresale una distancia diferente
    public static int[] sobreSaleTubos = {140,140,140};
    public static int sobreSaleTubosInicial = 140;
    // Esta variable es la separacion entre tubos que va restando poco a poco hasta quedarse en 0
    public static int separacionHeightTubosARestar = 140;
    // Este es el ultimo tubo del nivel
    public static int tuboFinal = 5;
    public static int posicionTuboFinal;
    // Cuenta los tubos que aparecen en la pantalla
    public static int contadorTubos = 0;
}
