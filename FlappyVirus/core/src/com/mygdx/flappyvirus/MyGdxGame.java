package com.mygdx.game;

import com.mygdx.game.Pantallas.PantallaGame;
import com.mygdx.game.Pantallas.PantallaNextLevel;
import com.mygdx.game.Pantallas.PantallaGameOver;
import com.mygdx.game.Pantallas.PantallaStart;
import com.mygdx.game.Diseños.Botones;
import com.mygdx.game.Objetos.*;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.Random;

public class MyGdxGame extends ApplicationAdapter {
	// Variables
	public static Random random;
	public static FitViewport fitViewport;
	public static SpriteBatch batch;
	public static Texture virus, top, bottom, pantallaStart, pantallaGameOver, pantallaNextLevel,
	nubes, cesped, meta, emojiVeryGood, moneda;

	public static float tiempo;
	public static int score, nivel;
	public static int pantallaWidth = 1000;
	public static int pantallaHeight = 600;

	// Stage 0 = boton start / 1 = gameOver / 2 = nextlevel
	public static Stage[] stages = new Stage[Botones.numeroBotones];
	public static Skin[] skins = new Skin[Botones.numeroBotones];
	public static TextButton botonStart;
	public static TextButton botonOver, botonNextLevel;

	public static Sprites spriteMeta;

	// Pantalla 0 = start , 1 = game, 2 = over, 3 = next level
	public static int pantalla = 0;

	// Creo las clases de los botones
//	public static com.mygdx.game.Diseños.Botones botonesStart = new com.mygdx.game.Diseños.Botones();
	public static Botones botonesStart = new Botones();
	public static Botones botonesOver = new Botones();
	public static Botones botonesNextLevel = new Botones();

	@Override
	public void create () {
		Assets.create();
		MyGdxGame.restartGame();
	}


	@Override
	public void render () {
		// Pantalla de start
		if (pantalla == 0) {
			PantallaStart.create();
		}

		// Pantall del juego
		else if (pantalla == 1) {
			PantallaGame.create();
		}

		// Pantalla de game over
		else if (pantalla == 2) {
			PantallaGameOver.create();
		}

		else if (pantalla == 3) {
			PantallaNextLevel.create();
		}
	}

	// Funcion que cambia el fitviewport al cambiar el tamaño de la pantalla
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		fitViewport.update(width, height, true);
		batch.setProjectionMatrix(fitViewport.getCamera().combined);
	}

	// Funcion que reinicia la posicion de los elementos
	public static void restartGame() {
		// Posicion del virus
		Virus.virusX = 64;
		Virus.virusY = 300;
		Virus.virusVelocidadY = 0;

		// Posicion de los tubos
		for (int i = 0; i < Tubos.numeroTubos; i++) {
			// Si es el primer tubo
			if (i == 0) {
				Tubos.tuboX[i] = pantallaWidth - 400;
			} else {
				Tubos.tuboX[i] = Tubos.tuboX[i-1] + Tubos.espacioEntreTubos;
			}
		}

		// Posicion de los tubos
		for (int i = 0; i < Tubos.numeroTubos; i++) {
			// Le pongo un numero random para que sobre salga diferente siempre
			Tubos.sobreSaleTubos[i] = (int) Math.floor(Math.random()*(Tubos.sobreSaleTubosInicial -( Tubos.sobreSaleTubosInicial - Tubos.separacionHeightTubosARestar +1))+(Tubos.sobreSaleTubosInicial - Tubos.separacionHeightTubosARestar));
			Tubos.tuboY[i] = -random.nextInt(Tubos.heightTubos - Tubos.sobreSaleTubos[i]);
		}

		// Posicion de las nubes
		for (int i = 0; i < Nubes.numeroNubes; i++) {
			// La primera nube
			if (i == 0) {
				Nubes.nubesX[i] = MyGdxGame.pantallaWidth / 2;
				// Lo reinicio a 1 el contador
				Tubos.contadorTubos = 1;
			} else {
				// Muestro las nubes con un espacio random
				int random = (int) Math.floor(Math.random()*(Nubes.nubesWidth-( Nubes.espacioEntreNubes + 1 ))+( Nubes.espacioEntreNubes ));
				Nubes.nubesX[i] = Nubes.nubesX[i-1] + random;
			}
		}
		// Posicion del cesped
		Cesped.cespedX[0] = 0;
		Cesped.cespedX[1] = 1000;
		tiempo = 0;
		MyGdxGame.score = 0;
		// Reinicio el contador de tubos a 3 que son los primeros 3 tubos que salen
		Tubos.contadorTubos = 3;
	}

	public void dispose() {
		Assets.dispose();
	}



}
