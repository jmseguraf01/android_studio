package com.mygdx.game.Pantallas;

import com.mygdx.game.*;
import com.mygdx.game.Diseños.Botones ;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class PantallaStart {
    // Funcion que pone la pantalla start
    public static void create() {
        MyGdxGame.batch.begin();
        Gdx.gl.glClearColor(255/255f,255/255f,255/255f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        MyGdxGame.batch.draw(MyGdxGame.pantallaStart, 0 , 0);
        MyGdxGame.batch.end();
        // Dibujo el stage del boton
        Botones.mostrarStage(0);
        // Si se le da click al boton de start
        MyGdxGame.botonStart.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                MyGdxGame.pantalla = 1;
                // Le quito la funcion del click al boton de stage
                Gdx.input.setInputProcessor(null);
            }
        });
    }
}
