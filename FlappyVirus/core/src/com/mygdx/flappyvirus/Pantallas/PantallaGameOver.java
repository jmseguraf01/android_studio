package com.mygdx.game.Pantallas;

import com.mygdx.game.*;
import com.mygdx.game.Diseños.Fuentes;
import com.mygdx.game.Diseños.Botones;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


public class PantallaGameOver {
    // La fuente de la pantalla gameover
    static Fuentes fuenteGameOver = new Fuentes(1.4f,1.4f);

    // Pantalla de cuando pierdes
    public static void create() {
        // Cambio el color y pongo el fondo
        Gdx.gl.glClearColor(107/255f,0,0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        MyGdxGame.batch.begin();
        MyGdxGame.batch.draw(MyGdxGame.pantallaGameOver, 0 , 0);

        // Dibujo el texto en pantalla
        fuenteGameOver.fuente.draw(MyGdxGame.batch, "Statistics:", MyGdxGame.pantallaWidth / 2 - 110, 170);
        fuenteGameOver.fuente.draw(MyGdxGame.batch, "- Time: " + String.format("%3.2f", MyGdxGame.tiempo), MyGdxGame.pantallaWidth / 2 - 110, 120);
        fuenteGameOver.fuente.draw(MyGdxGame.batch, "- Score: " + MyGdxGame.score , MyGdxGame.pantallaWidth / 2 - 110, 70);
        MyGdxGame.batch.end();

        // Pongo el boton y el stage
        Botones.mostrarStage(1);
        // Cuando se le da click al boton jugar
        MyGdxGame.botonOver.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                MyGdxGame.pantalla = 1;
                Gdx.input.setInputProcessor(null);
                MyGdxGame.restartGame();
            }
        });
    }
}
