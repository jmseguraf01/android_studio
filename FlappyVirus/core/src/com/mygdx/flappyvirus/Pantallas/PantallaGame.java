package com.mygdx.game.Pantallas;

import com.mygdx.game.*;
import com.mygdx.game.Objetos.Virus;
import com.mygdx.game.Objetos.Tubos;
import com.mygdx.game.Objetos.Nubes;
import com.mygdx.game.Objetos.Cesped;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.mygdx.game.Diseños.Fuentes;

public class PantallaGame {
    // La fuente que se utilizará en la pantalla
    static Fuentes fuenteGame = new Fuentes(1,1);



    // Pantalla del juego
    public static void create() {
        // Tiempo
        MyGdxGame.tiempo += Gdx.graphics.getDeltaTime();
        // glClearColor tienes que poner los colores y /225
        Gdx.gl.glClearColor(72/255f, 101/255f, 189/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Cuando pulso una tecla hago que suba el muñeco
        if (Gdx.input.justTouched()) {
            Virus.virusVelocidadY = 7f;
        }

        // Hago que vaya cayendo el virus
        Virus.virusY += Virus.virusVelocidadY;
        Virus.virusVelocidadY -= Virus.gravedad;
        MyGdxGame.batch.begin();

        // Cada vez el tubo va disminuyendo la posicion x
        for (int i = 0; i < Tubos.numeroTubos; i++) {
            Tubos.tuboX[i] -= 6;
        }

        // Cada vez se ma moviendo la nube
        Nubes.mover();
        // Compruebo si la nube se ha salido de la pantalla
        Nubes.comprobarSalePantalla();
        // Muevo el cesped y compruebo si sale de la pantalla
        Cesped.moverComprobar();

        // Siempre que no llegue al utlimo tubo
        if (Tubos.contadorTubos < Tubos.tuboFinal) {
            // Compruebo si el tubo ha salido de la pantalla los tubos
            for (int i = 0; i < Tubos.numeroTubos; i++) {
                if (Tubos.tuboX[i] < -Tubos.widthTubos) {
                    Tubos.tuboX[i] = MyGdxGame.pantallaWidth;
                    Tubos.contadorTubos++;
                    // Vuelvo a mostrar el tubo siempre que no haya llegado al tubo final
                    if (Tubos.contadorTubos < Tubos.tuboFinal) {
                        // Le pongo un numero random para que sobre salga diferente siempre
                        Tubos.sobreSaleTubos[i] = (int) Math.floor(Math.random() * (Tubos.sobreSaleTubosInicial - (Tubos.sobreSaleTubosInicial - Tubos.separacionHeightTubosARestar + 1)) + (Tubos.sobreSaleTubosInicial - Tubos.separacionHeightTubosARestar));
                        Tubos.tuboY[i] = -MyGdxGame.random.nextInt(Tubos.heightTubos - Tubos.sobreSaleTubos[i]);
                    } else {
                        MyGdxGame.spriteMeta.posicionY = (int) Tubos.tuboY[i] + Tubos.heightTubos;
                        MyGdxGame.spriteMeta.posicionX = (int) Tubos.tuboX[i];
                        Tubos.posicionTuboFinal = i;
                    }
                }
            }
        }

        // Cuando ya haya llegado al ultimo tubo voy moviendo la meta
        if (Tubos.contadorTubos  == Tubos.tuboFinal) {
            MyGdxGame.spriteMeta.posicionX -= 6;
            // Compruebo si ha ganado el nivel
            if (Tubos.tuboX[Tubos.posicionTuboFinal] == 0 || Tubos.tuboX[Tubos.posicionTuboFinal] == -2) {
                MyGdxGame.pantalla = 3;
            }
        }


        comprobarChocado();
        draw();
        MyGdxGame.batch.end();
    }


    // Funcion que comprueba si ha chocado
    private static void comprobarChocado() {
        for (int i = 0; i < Tubos.numeroTubos; i++) {
            // Si se choca
            if (Virus.virusX < Tubos.tuboX[i] + Tubos.widthTubos && Virus.virusX + Virus.widthVirus > Tubos.tuboX[i] && (Virus.virusY < Tubos.tuboY[i] + Tubos.heightTubos || Virus.virusY + Virus.widthVirus > Tubos.tuboY[i] + Tubos.separacionHeightTubos - Tubos.sobreSaleTubos[i] + Tubos.heightTubos) || Virus.virusY <= 0 || Virus.virusY >= MyGdxGame.pantallaHeight) {
                MyGdxGame.pantalla = 2;
            }
            // Compruebo si el virus pasa por medio de los tubos y le sumo 1 a score
            else if (Tubos.tuboX[i] == 0 || Tubos.tuboX[i] == -2) {
                MyGdxGame.score++;
            }
        }
    }

    // Dibuja en al pantalla
    private static void draw() {
        // Dibujo las nubes
        for (int i = 0; i < Nubes.numeroNubes; i++) {
            MyGdxGame.batch.draw(MyGdxGame.nubes, Nubes.nubesX[i], Nubes.nubesY, Nubes.nubesWidth, Nubes.nubesHeight);
        }
        // Dibujo el cesped
        for (int i = 0; i < Cesped.cespedX.length; i++) {
            MyGdxGame.batch.draw(MyGdxGame.cesped, Cesped.cespedX[i],0);
        }
        // Dibujo los tubos
        for (int i = 0; i < Tubos.numeroTubos; i++) {
            MyGdxGame.batch.draw(MyGdxGame.top, Tubos.tuboX[i], Tubos.tuboY[i] + Tubos.heightTubos + (MyGdxGame.pantallaHeight - Tubos.heightTubos - Tubos.sobreSaleTubos[i]) , Tubos.widthTubos, Tubos.heightTubos);
            MyGdxGame.batch.draw(MyGdxGame.bottom, Tubos.tuboX[i], Tubos.tuboY[i], Tubos.widthTubos, Tubos.heightTubos);
        }
        // Dibujo la meta si ya ha llegado al ultimo tubo
        if (Tubos.contadorTubos == Tubos.tuboFinal) {
            MyGdxGame.spriteMeta.render(MyGdxGame.batch);
        }
        // Dibujo el tiempo y el score pasados arriba a la derecha
        fuenteGame.fuente.draw(MyGdxGame.batch, "Time   " + String.format("%3.2f", MyGdxGame.tiempo), MyGdxGame.pantallaWidth - 140, MyGdxGame.pantallaHeight - 10);
        fuenteGame.fuente.draw(MyGdxGame.batch, "Score:   " + MyGdxGame.score, MyGdxGame.pantallaWidth - 140 , MyGdxGame.pantallaHeight - 45);
        fuenteGame.fuente.draw(MyGdxGame.batch, "Level: " + MyGdxGame.nivel, MyGdxGame.pantallaWidth - 140, MyGdxGame.pantallaHeight - 80);
        // Dibujo el virus
        MyGdxGame.batch.draw(MyGdxGame.virus, Virus.virusX, Virus.virusY, Virus.widthVirus,Virus.heightVirus);
    }
}
