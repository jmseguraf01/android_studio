package com.mygdx.game.Pantallas;

import com.mygdx.game.*;
import com.mygdx.game.Diseños.Fuentes;
import com.mygdx.game.Diseños.Botones;
import com.mygdx.game.Objetos.Tubos;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class PantallaNextLevel {
    // Fuente
    static Fuentes fuenteNextLevel = new Fuentes(2,2);
    static Fuentes fuenteNextLevel2 = new Fuentes(1.5f,1.5f);
    public static Sprites spriteMeta, spriteEmojiVeryGood;
    static boolean clickPrimeraVez;
    private static int newLevel;
    private static int tuboFinal;
    private static boolean first = true;

    // Muestra la pantalla de nextLevel
    public static void create() {
        // Si es la primera vez que se inicia esta pantalla
        if (first) {
            newLevel = MyGdxGame.nivel + 1;
            tuboFinal = Tubos.tuboFinal + 5;
            first = false;
        }
        // Cambio el color y pongo el fondo
        Gdx.gl.glClearColor(242/255f, 207/255f, 123/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        MyGdxGame.batch.begin();
        MyGdxGame.batch.draw(MyGdxGame.pantallaNextLevel,0,0);
        fuenteNextLevel.fuente.draw(MyGdxGame.batch, "Next objectives:", MyGdxGame.pantallaWidth / 2 - 220,MyGdxGame.pantallaHeight / 2 - 70);
        fuenteNextLevel2.fuente.draw(MyGdxGame.batch, "- Score: " + tuboFinal, MyGdxGame.pantallaWidth / 2 - 210,MyGdxGame.pantallaHeight / 2 - 145);
        drawSprites();
        MyGdxGame.batch.end();
        // Dibujo boton
        Botones.mostrarStage(2);
        clickPrimeraVez = true;
        // Le cambio el texto al boton
        MyGdxGame.botonNextLevel.setText("Start Level " + newLevel);
        // Click en el boton
        MyGdxGame.botonNextLevel.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                MyGdxGame.pantalla = 1;
                // Pongo esto porque se hace un bucle al darle click al boton
                if (clickPrimeraVez) {
                    // Le añadimos 5 al tubo del final del nivel
                    Tubos.tuboFinal += 5;
                    MyGdxGame.nivel += 1;
                    clickPrimeraVez = false;
                    first = true;
                }
                System.out.println(Tubos.tuboFinal);
                // Reiniciamos las variables que se refieren al tubo del final de nivel
                Tubos.posicionTuboFinal = 0;
                Tubos.contadorTubos = 0;
                MyGdxGame.restartGame();
                Gdx.input.setInputProcessor(null);
            }
        });
    }

    // Dibujan los sprites en la pantalla
    private static void drawSprites() {
        spriteMeta.render(MyGdxGame.batch);
        spriteEmojiVeryGood.render(MyGdxGame.batch);
    }

}
