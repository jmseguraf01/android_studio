package com.mygdx.game;

import com.mygdx.game.Pantallas.PantallaNextLevel;
import com.mygdx.game.Diseños.Botones;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.Random;

public class Assets {

    // Defino todos los assets
    static void create() {
        MyGdxGame.random = new Random();
        MyGdxGame.fitViewport = new FitViewport(MyGdxGame.pantallaWidth, MyGdxGame.pantallaHeight);
        MyGdxGame.batch = new SpriteBatch();

        // Cargo las imagenes
        MyGdxGame.virus = new Texture("virus.png");
        MyGdxGame.top = new Texture("top.png");
        MyGdxGame.bottom = new Texture("bot.png");
        MyGdxGame.pantallaStart = new Texture("start.png");
        MyGdxGame.pantallaGameOver = new Texture("over.png");
        MyGdxGame.pantallaNextLevel = new Texture("nextLevel.png");
        MyGdxGame.nubes = new Texture("nubes.png");
        MyGdxGame.cesped = new Texture("cesped.png");
        MyGdxGame.meta = new Texture("bandera.png");
        MyGdxGame.emojiVeryGood = new Texture("very_good.png");
        MyGdxGame.moneda = new Texture("moneda.png");
        // Variables
        MyGdxGame.score = 0;
        MyGdxGame.tiempo = 0;
        MyGdxGame.nivel = 1;
        // Creo los sprites
        MyGdxGame.spriteMeta = new Sprites(MyGdxGame.meta, 0,0,6, 1, 5f,2);
        PantallaNextLevel.spriteMeta  = new Sprites(MyGdxGame.meta, 820,265,6,1,1f,2);
        PantallaNextLevel.spriteEmojiVeryGood = new Sprites(MyGdxGame.emojiVeryGood, MyGdxGame.pantallaWidth - 250,MyGdxGame.pantallaHeight - 200,16,1,1f,2);


        // Creo los stage, skin y botones de start y de game over
        for (int i = 0; i < Botones.numeroBotones; i++) {
            MyGdxGame.stages[i] = new Stage(MyGdxGame.fitViewport);
            MyGdxGame.skins[i] = new Skin(Gdx.files.internal("skin/uiskin.json"));
        }
        // Creo los botones y le pongo los atributos
        MyGdxGame.botonStart = MyGdxGame.botonesStart.crearBotones("Play Game", 200,100, MyGdxGame.pantallaWidth / 2 - 125, MyGdxGame.pantallaHeight / 2 + 20, 1.7f, MyGdxGame.stages[0], MyGdxGame.skins[0]);
        MyGdxGame.botonOver = MyGdxGame.botonesOver.crearBotones("Play Again", 200,100, MyGdxGame.pantallaWidth / 2 - 125, MyGdxGame.pantallaHeight / 2 + 20,1.7f, MyGdxGame.stages[1], MyGdxGame.skins[1]);
        MyGdxGame.botonNextLevel = MyGdxGame.botonesNextLevel.crearBotones("Next Level", 200,100,MyGdxGame.pantallaWidth / 2 - 125, MyGdxGame.pantallaHeight / 2 - 20,1.7f,  MyGdxGame.stages[2], MyGdxGame.skins[2]);
    }

    // Le quito a la memoria estos assets
    static void dispose() {
        MyGdxGame.batch.dispose();
        MyGdxGame.virus.dispose();
        MyGdxGame.top.dispose();
        MyGdxGame.bottom.dispose();
        MyGdxGame.pantallaStart.dispose();
        MyGdxGame.pantallaGameOver.dispose();
        MyGdxGame.pantallaNextLevel.dispose();
        MyGdxGame.nubes.dispose();
        MyGdxGame.cesped.dispose();
        MyGdxGame.meta.dispose();
    }

}
