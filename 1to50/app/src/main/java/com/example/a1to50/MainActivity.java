package com.example.a1to50;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    // Cronometro
    Chronometer cronometroJuego;
    // Array de botones
    Button[] botones;
    // Array para ver si el boton ha sido asignado
    Boolean[] asignados = new Boolean[50];
    Random random = new Random();
    int numeroActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cronometroJuego = findViewById(R.id.cronometroJuego);

        // Consigo el id de cada uno de los botones
        botones = new Button[25];
        for (int i = 0; i < 25; i++) {
            botones[i] = findViewById(getResources().getIdentifier("boton" + i, "id", getPackageName()));
            System.out.println(botones[i]);
        }

        initGame();

    }

    // Funcion que inicia el juego
    void initGame() {
        numeroActual = 1;

        // Actualizo el estado de asignados
        for (int i = 0; i < 50; i++) {
            asignados[i] = false;
        }

        // Por cada boton
        for (int i = 0; i < 25; i++) {
            botones[i].setVisibility(botones[i].VISIBLE);
            // Hago un while para que se pongan los 25 numeros de forma random
            while (true) {
                // Obtengo un numero random entre 0 y 25
                int num = random.nextInt(25);
                // Siempre que el boton no haya sido asignado anteriormente
                if (!asignados[num]) {
                    botones[i].setText(String.valueOf(num+1));
                    botones[i].setBackgroundColor(Color.parseColor("#4466aa"));
                    asignados[num] = true;
                    break;
                }
            }
        }


    }

    // Funcion de cuando se clicka cualquier boton
    public void clickButton(View view) {
        Button botonClickado = (Button) view;

        // Cuando se da al numero que corresponde
        if (botonClickado.getText().equals(String.valueOf(numeroActual))) {
            System.out.println("cierto");

            // Cuando es el primer numero
            if (numeroActual == 1) {
                cronometroJuego.setBase(SystemClock.elapsedRealtime());
                cronometroJuego.start();
            }

            // Cuando es mas pequeño de 25
            if (numeroActual <= 25) {
                while (true) {
                    int num = random.nextInt(50);
                    if (!asignados[num]) {
                        botonClickado.setText(String.valueOf(num+1));
                        botonClickado.setBackgroundColor(Color.parseColor("#335599"));
                        asignados[num] = true;
                        break;
                    }
                }
            }

            // Cuando ha llegado a 50
            else if (numeroActual == 50) {
                cronometroJuego.stop();
                new AlertDialog.Builder(this)
                        .setTitle("¡COMPLETADO!")
                        .setMessage("Tiempo: " + cronometroJuego.getContentDescription())
                        .setPositiveButton("Jugar de nuevo", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                initGame();
                            }
                        })
                        .create()
                        .show();

            }

            else {
                botonClickado.setVisibility(botonClickado.INVISIBLE);
            }

            numeroActual++;
        }
    }

}
